SimpleNavigation::Configuration.run do |navigation|
  navigation.selected_class = 'active'
  navigation.items do |primary|
    primary.dom_class = 'navbar-nav'
    primary.item :home, t("layouts.navigation.home").html_safe, root_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link' }
    primary.item :objects, t("layouts.navigation.objects").html_safe, samples_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link' }
    primary.item :language, t("layouts.navigation.language").html_safe, "#", html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link', :"data-toggle" => "modal", :"data-target"=>"#langModal"}
    #Signin & Signout
    if @current_user.user?
      primary.item :admin, t("layouts.navigation.admin").html_safe, rails_admin_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link last-right', :style => 'margin-right: 190px' }, :if => Proc.new { current_user.admin? }
      primary.item :account, t("layouts.navigation.account").html_safe, account_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link last-right', :style => 'margin-right: 100px' }
      primary.item :logout, t("layouts.navigation.signout").html_safe, destroy_user_session_path, :method => 'delete',html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link last-right' }
    else
      primary.item :signup, t("layouts.navigation.signup").html_safe, new_user_registration_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link last-right', :style => 'margin-right: 90px' }
      primary.item :signin, t("layouts.navigation.signin").html_safe, new_user_session_path, html: { :class => 'nav-item', :id=>'', }, link_html: { :class => 'nav-link last-right'}
    end
  end
end
