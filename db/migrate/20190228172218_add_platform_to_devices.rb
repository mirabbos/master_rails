class AddPlatformToDevices < ActiveRecord::Migration[5.2]
  def change
    add_column :devices, :platform, :string
  end
end
