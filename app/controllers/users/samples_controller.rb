class Users::SamplesController < ApplicationController
  before_action :set_sample, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /samples
  def index
    @samples = Sample.where(:user_id => current_user.id)
  end

  def new
    @sample = Sample.new
  end

  # GET /samples/1
  def show
    #render json: @sample
  end

  #Edit /samples/:id/
  def edit

  end

  # POST /samples
  def create
    @sample = Sample.new(sample_params)
    @sample.user_id = @current_user.id
    if @sample.save
      redirect_to samples_path
    else
      redirect_to sample_new_path
    end
  end

  # PATCH/PUT /samples/1
  def update
    if @sample.update(sample_params)
      redirect_to samples_path
      #redirect_to request_user_show_path(@request)
    else
      redirect_back_or_default(root_path, :alert => "Some thing wrong!.")
    end
  end

  # DELETE /samples/1
  def destroy
    @sample.destroy
    redirect_to samples_path
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_sample
    @sample = Sample.find(params[:sample_id])
  end

  # Only allow a trusted parameter "white list" through.
  def sample_params
    if Rails::VERSION::MAJOR < 4
      params[:sample].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:sample).permit(Sample.new.attributes.keys - protected_attrs, :_destroy)
    end
  end
end
