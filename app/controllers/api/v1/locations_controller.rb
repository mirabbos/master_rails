class Api::V1::LocationsController < ApiController
  before_action :set_device, only: [:create]

  # PATCH/PUT /devices/:coords
  def create
    @location = Location.find_or_create_by(:device_id => @device.id)

    if @location.update(location_params)

      render json: {
                 status: 200,
                 message: "Response successfully sent!",
                 params: location_params
             }.to_json
    else
      render json: {
                 status: 400,
                 message: "User device location failed to save!",
                 error: @location.errors
             }.to_json
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_device
    @device = Device.find_by(params[:mac_address])
  end
  # Never trust parameters from the scary internet, only allow the white list through.
  def location_params
    if Rails::VERSION::MAJOR < 4
      params[:coords].permit(:_destroy)
    else
      params.require(:coords).permit(:speed, :longitude, :latitude, :accuracy, :heading, :altitude, :altitudeAccuracy, :mac_address)
    end
  end
end