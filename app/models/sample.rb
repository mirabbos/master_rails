class Sample < ApplicationRecord
  belongs_to :user

  # Backend validations
  validates :error_name,
            :presence => true
  validates :description,
            :presence => true

  def error_name
    if self.name.nil?
      errors.add(:name, "Please enter the name of the object.")
    end
  end
end