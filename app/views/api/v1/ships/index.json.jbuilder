json.array! @ships do |ship|
  json.extract! ship, :id, :name, :description
end